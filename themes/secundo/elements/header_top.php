<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?php echo Localization::activeLanguage()?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    
    <?php echo $html->css($view->getStylesheet('style.less'))?>
    <?php Loader::element('header_required', array('pageTitle' => $pageTitle));?>
    
    
    <link href="<?php echo $view->getThemePath();?>/css/flexslider.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=News+Cycle:400,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    
    
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style')
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle)
        }
    </script>
    
        
    <?php if (!$c->isEditMode()):?>
	<script src="<?php echo $view->getThemePath();?>/js/jquery.js"></script>
	<script src="<?php echo $view->getThemePath();?>/bootstrap/js/bootstrap.min.js"></script>
	<?php endif;?>
	
</head>
<body>

<div class="<?php echo $c->getPageWrapperClass()?>">