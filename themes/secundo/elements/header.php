<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php');
?>

<div class="<?php echo $c->getPageWrapperClass()?>">
<div class="navbar navbar-static-top">
	<div class="navbar-inner">
		<div class="container">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="/"><img src="<?php echo $view->getThemePath();?>/images/content/logo.png" alt=" "></a>

			<div class="nav-collapse">
			<?php
				$nav = new GlobalArea('Navigate');
				$nav->display($c);
			?>
			</div>
		</div>
	</div>
</div>