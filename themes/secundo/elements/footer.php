
<footer id="footerContainer">
    <div class="patDark">
        <div class="container">
            <div class="row-fluid">
                <div class="span3 doCenter">
				<?php
					/*  
						<h3 class="vwhite vsmall">CONTACT</h3>
	                    <p>
	                        <em>Address:</em> St Petersburg, Russia<br>
	                        <em>Email:</em> <a href="mailto:seafolk@ya.ru">seafolk@ya.ru</a>
	                    </p>
                    */
					$nav = new GlobalArea('FooterContainerCol1');
					$nav->display($c);
				?>
                </div>
                <div class="span3 doCenter">
                </div>
                <div class="span3 doCenter">
                </div>
                <div class="span3 doCenter">
                </div>
            </div>
        </div>
    </div>
</footer>

</div>

	<script src="<?php echo $view->getThemePath();?>/js/jquery.easing.js"></script>
	<script src="<?php echo $view->getThemePath();?>/js/jquery.quicksand.js"></script>
	<script src="<?php echo $view->getThemePath();?>/js/jquery.flexslider-min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        /***  Footer Back to Top link ***/
        $('a.toTop').click(function(){
             $('html, body').animate({scrollTop: '0px'}, 300);
             return false;
        });
    });

</script>

<?php  Loader::element('footer_required'); ?>

</body>
</html>