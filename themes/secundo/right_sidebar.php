<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>

<?php
	$ba = new Area('ContainerBefore');
	$ba->display($c);
?>

<div class="patStd">
    <div class="container">
		<div class="row-fluid">
			<div class="span9">
				<!-- blog left -->
				<div class="rightPadd20">
					<?php
						$a = new Area('Container');
						$a->display($c);
					?>
				</div>
				<!-- blogContainer -->

			</div>
			<div class="span3">
				<!-- blog right -->
				<?php
						$r = new Area('RightContainer');
						$r->display($c);
				?>
			</div>
		</div>
		<!-- row-fluid -->
    </div>
</div>

<?php
	$ba = new Area('ContainerAfter');
	$ba->display($c);
?>
			
<div class="patStd nomrg">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 doRight">
                <br><br><br>
                <a href="#" class="arrowIcon vsmall toTop">BACK TO TOP<i class="arrow-toTop"></i></a>
            </div>
        </div>
    </div>
</div>

<?php  $this->inc('elements/footer.php'); ?>