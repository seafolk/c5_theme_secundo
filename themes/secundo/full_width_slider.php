<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>

<?php
	$ba = new Area('ContainerBefore');
	$ba->display($c);
?>

<?php /* 
<div class="container">
    <div class="row-fluid">
        <div class="span12 doCenter">
            <h1 class="big">WE ARE SECUNDO. WE MAKE GREAT STUFF.</h1>
        </div>
    </div>
</div>
*/?>

<div class="patBlue nomrg">
	<div class="container">
		<div class="row-fluid">
			<div class="span3 doCenter">
				<a href="#" class="opacityLess">
					<img src="<?php echo $view->getThemePath();?>/images/content/services-small-1.png" alt=" ">

					<h3 class="vmedium vwhite nobtmrg">BRAND<br>
                    <span class="vwhite vitalic">creating</span></h3>

					<span class="hover_el"></span>
				</a>
			</div>
			<div class="span3 doCenter">
				<a href="#" class="opacityLess">
					<img src="<?php echo $view->getThemePath();?>/images/content/services-small-2.png" alt=" ">

					<h3 class="vmedium vwhite nobtmrg">WEB<br>
                    <span class="vwhite vitalic">development</span></h3>

					<span class="hover_el"></span>
				</a>
			</div>
			<div class="span3 doCenter">
				<a href="#" class="opacityLess">
					<img src="<?php echo $view->getThemePath();?>/images/content/services-small-3.png" alt=" ">

					<h3 class="vmedium vwhite nobtmrg">MOBILE<br>
                    <span class="vwhite vitalic">apps</span></h3>

					<span class="hover_el"></span>
				</a>
			</div>
			<div class="span3 doCenter">
				<a href="#" class="opacityLess">
					<img src="<?php echo $view->getThemePath();?>/images/content/services-small-4.png" alt=" ">

					<h3 class="vmedium vwhite nobtmrg">ONLINE<br>
                    <span class="vwhite vitalic">marketing</span></h3>

					<span class="hover_el"></span>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="patStd">
    <div class="container">
			<?php
				$a = new Area('Container');
				$a->display($c);
			?>
    </div>
</div>

<?php
	$ba = new Area('ContainerAfter');
	$ba->display($c);
?>

<div class="patStd">
    <div class="container">
        <div class="row-fluid">
            <div class="span8">
                <div class="rightPadd20">
                <h2 class="crossLine"><span>ИЗ МОЕГО <strong>БЛОГА</strong></span></h2>
					<?php
						$bb = new Area('ContainerBlogList');
						$bb->display($c);
					?>
                </div>
                <!-- / rightPadd20 -->
            </div>
            <div class="span4">
                <h2 class="crossLine"><span>ПОСЛЕДНИЕ <strong>ТВИТЫ</strong></span></h2>

                <div class="tweets"></div>

            </div>
        </div>
        <!-- / row-fluid -->

    </div>
</div>

<div class="patStd nomrg">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 doRight">
                <br><br><br>
                <a href="#" class="arrowIcon vsmall toTop">BACK TO TOP<i class="arrow-toTop"></i></a>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo $view->getThemePath();?>/js/jquery.flexslider-min.js"></script>

<script type="text/javascript">
//     $(document).ready(function () {
//       /* twitter */
//       $('.tweets').tweet({
//           template: "{text} {time}",
//           //li_class: " ",
//           twitter_api_url: 'twitter/proxy/twitter_proxy.php'
//       });
//     });
</script>

<?php  $this->inc('elements/footer.php'); ?>