<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');
?>

<?php
	$ba = new Area('ContainerBefore');
	$ba->display($c);
?>

<div class="patStd">
    <div class="container">
			<?php
				$a = new Area('Container');
				$a->display($c);
			?>
    </div>
</div>

<?php
	$ba = new Area('ContainerAfter');
	$ba->display($c);
?>
			
<div class="patStd nomrg">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 doRight">
                <br><br><br>
                <a href="#" class="arrowIcon vsmall toTop">BACK TO TOP<i class="arrow-toTop"></i></a>
            </div>
        </div>
    </div>
</div>

<?php  $this->inc('elements/footer.php'); ?>