<?php       
namespace Concrete\Package\Secundo;

use BlockType;
use Package;
use PageType;
use PageTemplate;
use PageTheme;
use Concrete\Core\Attribute\Key\Category;
use Concrete\Core\Attribute\Key\FileKey;
use Concrete\Core\File\Image\Thumbnail\Type\Type as ThumbnailType;
use Concrete\Core\Page\Type\PublishTarget\Type\Type as PublishTargetType;
use Concrete\Core\Asset\AssetList;
use View;

use CollectionAttributeKey;
use Concrete\Core\Attribute\Type as AttributeType;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package
{

	protected $pkgHandle = 'secundo';
	protected $appVersionRequired = '5.7';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("...");
	}
	
	public function getPackageName() {
		return t("Secundo - Theme");
	}
	
	public function install()
	{
	  	$pkg = parent::install();
	  	
	  	$this->installThemes($pkg);
	  	
	  	// install page types
	  	$this->installPageType($pkg, "full", 'Full');
	  	
	  	// Add attributes
	  	// thumbnail
	  	$imgAttr = AttributeType::getByHandle('image_file');
	  	$thumb = CollectionAttributeKey::getByHandle('thumbnail');
	  	if(!is_object($thumb)) {
	  		CollectionAttributeKey::add($imgAttr,
	  				array(
	  						'akHandle' => 'thumbnail',
	  						'akName' => t('Thumbnail Image'),
	  				),$pkg);
	  	}
	  	
	  	// tags
	  	// TODO
	}
	
	private function installThemes($pkg) {
		PageTheme::add('secundo', $pkg);
	}
	
	private function installPageType($pkg, $handle, $title) {
		if (PageTemplate::getByHandle($handle) && !PageType::getByHandle($handle)) {
			$pageType = PageType::add(array(
					'handle'                => $handle,
					'name'                  => t($title),
					'defaultTemplate'       => PageTemplate::getByHandle($handle),
					'ptIsFrequentlyAdded'   => 1,
					'ptLaunchInComposer'    => 0
			), $pkg);
			$target       = PublishTargetType::getByHandle('all');
			$targetConfig = $target->configurePageTypePublishTarget($pageType, array(
					'ptID' => $pageType->getPageTypeID()
			));
	
			// Set configured publish target
			$pageType->setConfiguredPageTypePublishTargetObject($targetConfig);
		}
	}
}