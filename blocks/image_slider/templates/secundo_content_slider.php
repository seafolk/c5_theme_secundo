<?php defined('C5_EXECUTE') or die("Access Denied.");
$navigationTypeText = ($navigationType == 0) ? 'arrows' : 'pages';
$c = Page::getCurrentPage();
if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
        <div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php  } else { ?>
<script>
$(document).ready(function(){
    $(function () {
            $("#ccm-image-slider-<?php echo $bID ?>").flexslider({
                animation:"slide",
                controlNav:true,
                directionNav:false,
                smoothHeight:true,
                slideshowSpeed:15000
            });
    });
});
</script>

<div class="patDark nomrg">
    <div class="container">
		<div class="flexslider" id="ccm-image-slider-<?php echo $bID ?>">
        <?php if(count($rows) > 0) { ?>
        <ul class="slides" id="ccm-image-slider-<?php echo $bID ?>">
            <?php foreach($rows as $row) { ?>
            
            
            <li>
                    <div class="row-fluid">
                        <div class="span4">
						<?php if($row['title']) { ?>
                    	<h2 class="great vwhite topMrg"><?php echo $row['title'] ?></h2>
                    <?php } ?>
                            <p class="vmedium">
                               <?php echo $row['description'] ?>
                            </p>

                            <?php if($row['linkURL']) { ?>
			                    <a href="<?php echo $row['linkURL'] ?>" class="btn vgray vlarge">MORE INFO</a>
			                <?php } ?>
                            <br><br><br>
                        </div>
                        <div class="span8 doRight">
                            <?php
			                $f = File::getByID($row['fID'])
			                ?>
			                <?php if(is_object($f)) {
			                    $tag = Core::make('html/image', array($f, false))->getTag();
			                    if($row['title']) {
			                    	$tag->alt($row['title']);
			                    }else{
			                    	$tag->alt("slide"); 
			                    }
			                    $tag->class("contentSliderPhoto topMrg");
			                    // <img src="images/content/home-content-slider-1.png" alt=" " width="558" height="343" 
			                    // class="contentSliderPhoto topMrg"/>
                            	print $tag; ?>
			                <?php } ?>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="ccm-image-slider-placeholder">
            <p><?php echo t('No Slides Entered.'); ?></p>
        </div>
        <?php } ?>
		</div>
	</div>
</div>
<?php } ?>
