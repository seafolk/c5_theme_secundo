<?php defined('C5_EXECUTE') or die("Access Denied.");

/* <link href="css/foundation.css" rel="stylesheet" type="text/css" />
<link href="css/twentytwenty.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.event.move.js"></script>
<script src="js/jquery.twentytwenty.js"></script> */

$c = Page::getCurrentPage();
if (is_object($f)) {
	/*
    if ($maxWidth > 0 || $maxHeight > 0) {
        $im = Core::make('helper/image');
        $thumb = $im->getThumbnail(
            $f,
            $maxWidth,
            $maxHeight
        ); //<-- set these 2 numbers to max width and height of thumbnails
        $tag = new \HtmlObject\Image();
        $tag->src($thumb->src)->width($thumb->width)->height($thumb->height);
    } else {
        $image = Core::make('html/image', array($f));
        $tag = $image->getTag();
    }
    $tag->addClass('ccm-image-block img-responsive bID-'.$bID);
    if ($altText) {
        $tag->alt(h($altText));
    }
    if ($title) {
        $tag->title(h($title));
    }
    if ($linkURL):
        print '<a href="' . $linkURL . '">';
    endif;

    print $tag;

    if ($linkURL):
        print '</a>';
    endif;
    */
	?>
	<div class="ccm-image-block img-responsive bID-<?php echo $bID;?>">
		<img src="<?php print $imgPath["default"];?>">
  		<img src="<?php print $imgPath["hover"];?>">
	</div>
	<?php
} else if ($c->isEditMode()) { ?>

    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Block.')?></div>

<?php } ?>

<?php if(is_object($foS)) { ?>	
<script>
$(function() {
	<?php /*
		$(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.7});
    	$(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0.3, orientation: 'vertical'});
    */?>
	
    $('.bID-<?php print $bID;?>').twentytwenty();
});
</script>
<?php } ?>
