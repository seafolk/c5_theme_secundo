<?php defined('C5_EXECUTE') or die("Access Denied.");
$linkCount = 1;
$faqEntryCount = 1; ?>
    <?php if (count($rows) > 0) { ?>

			<div class="patBright nomrg">
			    <div class="container">
			        <div class="row-fluid">
			            <div class="span12">
			                <div class="flexslider simple" id="slider<?php echo $bID?>">
			                    <ul class="slides">
			                    
			                        <?php foreach ($rows as $row):?>
			                        <li>
			
			                            <blockquote>
			                                <p><?php echo $row['description'] ?></p>
			                                <span class="author"><?php echo $row['title'] ?></span>
			                            </blockquote>
			                        </li>
								    <?php $faqEntryCount++;?>
								    <?php endforeach;?>
								    
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
        
        <script type="text/javascript">

		    $(window).load(function () {
		
		        /* testimonials slider */
		
		        $('#slider<?php echo $bID?>').flexslider({
		            animation:"slide",
		            controlNav:false,
		            directionNav:true,
		            smoothHeight:true,
		            slideshowSpeed:8000,
		            slideshow: false
		        });
		
		    });
		
	</script>
    <?php } else { ?>
        <div class="ccm-faq-block-links">
            <p><?php echo t('No Faq Entries Entered.'); ?></p>
        </div>
    <?php } ?>
