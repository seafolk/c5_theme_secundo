<?php
/**
 * Attributes:
 *	thumbnail - img 180x180px
 *	tags - TODO
 *
 **/

	use Concrete\Attribute\Select\OptionList;
	
	$imgHelper = Loader::Helper('image');
	
	$aPages = array();
	$tags = array();
	
	foreach ($pages as $i=>$cobj){
		$date = $cobj->getCollectionDatePublic(DATE_APP_GENERIC_MDY_FULL);
		$author = $cobj->getVersionObject()->getVersionAuthorUserName();

		$p = array(
			'descr' => $cobj->vObj->cvDescription,
			'link' => $nh->getLinkToCollection($cobj),
			'img' =>  '',
			'title' => $cobj->getCollectionName(),
			'tags' => array()
		);
		
		$file = $cobj->getAttribute("thumbnail");
		if (is_object($file)) {
			$p['img'] = $imgHelper->getThumbnail($file, 180, 180)->src; 
		}else{
			$p['img'] = "https://placeholdit.imgix.net/~text?txtsize=33&txt=180x180&w=180&h=180";
		}
				
		// tags
		$options = $cobj->getAttribute("tags");
		if ($options instanceof OptionList && $options->count() > 0){
			foreach($options as $option) {
				$tags[$option->getSelectAttributeOptionValue()] = $option->getSelectAttributeOptionValue();
				
				$p['tags'][] = $option->getSelectAttributeOptionValue();
			}
		}
		
		$aPages[] = $p;
	}
?>
                <ul class="filterPortfolio">
                    <li class="first">ТИП:</li>
                    <li class="active"><a href="#" >ВСЕ</a></li>
                    <?php foreach ($tags as $tagId=>$tagName):?>
                    	<li data-filter="<?php echo $tagId?>"><a href="#"><?php echo $tagName?></a></li>
                    <?php endforeach;?>
                </ul>
                <div class="clearfix"></div>

                <div class="galleryContainer">
                    <ul class="thumbnails galleryPortfolio clearfix" id="portfolios1">
                    <?php foreach ($aPages as $i=>$page):?>
	                        <li class="span3 doCenter" data-id="<?php echo $i+1?>" data-filter="<?php echo implode(' ', $page['tags']);?>">
                            <a href="<?php echo $page['link']?>" class="thumbnail" >
                                <span class="circleFrame doCenter" style="background: url(<?php echo $page['img']?>) no-repeat top center;">
                                    <img src="<?php echo $page['img']?>" alt="">
                                </span>
                             </a>
                             <h3><a href="<?php echo $page['link']?>" class="vmedium vorange"><?php echo $page['title']?></a></h3>
                             <?php foreach ($page['tags'] as $tag):?>
                             	<a class="btn"><?php echo $tag?></a>
                             <?php endforeach;?>
                        </li>
                   <?php endforeach; ?>
  </ul>
</div>
<!-- / galleryContainer -->

<script type="text/javascript">

    $(document).ready(function () {

        /*** Quicksand ***/
        var p = $('#portfolios1');
        var f = $('.filterPortfolio');
        var data = p.clone();
        f.find('a').click(function () {
            var link = $(this);
            var li = link.parents('li');
            if (li.hasClass('active')) {
                return false;
            }

            f.find('li').removeClass('active');
            li.addClass('active');

            //quicksand
            var filtered = li.data('filter') ? data.find('li[data-filter~="' + li.data('filter') + '"]') : data.find('li');
            
            p.quicksand(filtered, {
                duration:800,
                easing:'easeInOutQuad'
            }, function () { // callback function

            });
            return false;
        });


        /***  Footer Back to Top link ***/
        $('a.toTop').click(function(){
             $('html, body').animate({scrollTop: '0px'}, 300);
             return false;
        });
    });

</script>