<?php
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$imgHelper = Loader::Helper('image');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */

	foreach ($pages as $page):

		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);

        $date = $page->getCollectionDatePublic();
		?>
        

                    <div class="row-fluid">
                        <div class="span1">
                            <span class="date"><?php echo $dh->formatCustom('D', $date)?> <em><?php echo $dh->formatCustom('d', $date)?></em></span>
                        </div>
                        <div class="span11">
                            <h3><a href="<?php   echo $url; ?>" target="<?php echo $target;?>" class="vmedium vorange"><?php   echo $title; ?></a></h3>

                            <p><?php  echo $description; ?></p>
                        </div>
                    </div>

	<?php  endforeach; ?>

    <?php  if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php  echo $noResultsMessage?></div>
    <?php  endif;?>
