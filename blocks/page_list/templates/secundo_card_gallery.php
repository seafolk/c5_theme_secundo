<?php
/**
 * Attributes:
 *	thumbnail - img 180x180px
 *	tags - TODO
 *
 **/

	use Concrete\Attribute\Select\OptionList;
	
	$imgHelper = Loader::Helper('image');
	
	$aPages = array();
	$tags = array();
	
	foreach ($pages as $i=>$cobj){
		$date = $cobj->getCollectionDatePublic(DATE_APP_GENERIC_MDY_FULL);
		$author = $cobj->getVersionObject()->getVersionAuthorUserName();

		$p = array(
			'descr' => $cobj->getCollectionDescription(),
			'link' => $nh->getLinkToCollection($cobj),
			'img' =>  '',
			'title' => $cobj->getCollectionName(),
			'tags' => array()
		);
		
		$file = $cobj->getAttribute("thumbnail");
		if (is_object($file)) {
			$p['img'] = $imgHelper->getThumbnail($file, 180, 180)->src; 
		}else{
			$p['img'] = "https://placeholdit.imgix.net/~text?txtsize=33&txt=180x180&w=180&h=180";
		}
				
		// tags
		$options = $cobj->getAttribute("tags");
		if ($options instanceof OptionList && $options->count() > 0){
			foreach($options as $option) {
				$tags[$option->getSelectAttributeOptionValue()] = $option->getSelectAttributeOptionValue();
				
				$p['tags'][] = $option->getSelectAttributeOptionValue();
			}
		}
		
		$aPages[] = $p;
	}
?>

<div class="patStd">
	<div class="container">

		<?php /*
		<h2 class="crossLine"><span>Последние <strong>работы</strong></span></h2>
		*/?>
		<?php if ($pageListTitle): 
			$arrTitle = explode(' ', $pageListTitle);
			if(count($arrTitle)===2){
				$arrTitle[1] = '<strong>' . $arrTitle[1] . '</strong>';
			}
		?>
        <div class="ccm-block-page-list-header">
            <h2 class="crossLine"><span><?php echo implode(' ', $arrTitle);?></span></h2>
        </div>
    	<?php endif; ?>
		
		<div class="row-fluid">
		<?php foreach ($aPages as $i=>$page):?>
			<div class="span6">
				<div class="boxShad">
					<div class="row-fluid">
						<div class="span6 doCenter">
							<a href="<?php echo $page['link']?>">
                                <span class="circleFrame doCenter" style="background: url(<?php echo $page['img']?>) no-repeat top center;">
                                    <img src="<?php echo $page['img']?>" alt="" width="150px" height="150px">
                                </span>
							</a>
						</div>
						<div class="span6">
							<h3><a href="<?php echo $page['link']?>" class="vmedium vorange"><?php echo $page['title']?></a></h3>
							<?php foreach ($page['tags'] as $tag):?>
                             	<a class="btn"><?php echo $tag?></a>
                             <?php endforeach;?>

							<p><?php echo substr($page["descr"], 0, 250);?>...</p>
						</div>
					</div>


					<span class="lftShad"></span>
					<span class="midShad"></span>
					<span class="rtShad"></span>
				</div>
			</div>
						
			<?php if($i%2 && $i!=count($aPages)-1):?>
				</div><div class="row-fluid">
			<?php endif;?>
			
		<?php endforeach; ?>
		</div>
	<!-- / container -->
</div>
<!-- / patStd -->