<?php 
defined('C5_EXECUTE') or die("Access Denied.");
$th = Loader::helper('text');
$imgHelper = Loader::Helper('image');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */
?>

    <?php  if ($rssUrl): ?>
        <a href="<?php  echo $rssUrl ?>" target="_blank" class="ccm-block-page-list-rss-feed"><i class="fa fa-rss"></i></a>
    <?php  endif; ?>

    <?php  foreach ($pages as $page):

		// Prepare data for each page being listed...
        $buttonClasses = 'ccm-block-page-list-read-more';
        $entryClasses = 'ccm-block-page-list-page-entry';
		$title = $th->entities($page->getCollectionName());
		$url = $nh->getLinkToCollection($page);
		
		$target = ($page->getCollectionPointerExternalLink() != '' && $page->openCollectionPointerExternalLinkInNewWindow()) ? '_blank' : $page->getAttribute('nav_target');
		$target = empty($target) ? '_self' : $target;
		
		$description = $page->getCollectionDescription();
		$description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
		$description = $th->entities($description);
        
		$thumbnailSrc = false;
        $thumbnail = $page->getAttribute("thumbnail");
        if (is_object($thumbnail)) {
        	$thumbnailSrc = $imgHelper->getThumbnail($thumbnail, 652, 292)->src;
        }


        $date = $page->getCollectionDatePublic();
        
        $comments = 0;
		if(method_exists($entryController,'getCommentCountString')) {
			$comments = $entryController->getCommentCountString('%s '.t('Comment'), '%s '.t('Comments'));
		}
		
		?>
        
        		<?php if($thumbnailSrc):?>
        			<a href="<?php echo $url; ?>"><img src="<?php echo $thumbnailSrc?>" alt="<?php echo $title; ?>" class="simpleFrame post-photo"></a>
				<?php endif;?>
				
					<h2 class="vmedium vorange"><a href="<?php echo $url; ?>" target="<?php echo $target;?>"><?php echo $title; ?></a></h2>

					<p class="vgray"><?php echo $dh->formatCustom('d.m.Y', $date)?>  |  <!-- <a href="#">Personal</a>   |-->  <a href="#"><?php echo $comments?> Comments</a></p>

					<p><?php  echo $description; ?></p>

					<hr class="simple">
					

	<?php  endforeach; ?>

    <?php  if (count($pages) == 0): ?>
        <div class="ccm-block-page-list-no-pages"><?php  echo $noResultsMessage?></div>
    <?php  endif;?>


<?php  if ($showPagination): ?>
    <?php  echo $pagination;?>
<?php  endif; ?>
